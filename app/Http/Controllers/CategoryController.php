<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;

class CategoryController extends Controller
{

    const UPLOAD_DIR = '/uploads/categories/';

    public function index()
    {
        $paginatePerPage = 10;
        $pageNumber = request('page');
        if($pageNumber > 1){
            $serial = ($pageNumber * $paginatePerPage) - $paginatePerPage;
        }else{
            $serial = 0;
        }
//        $categories = Category::all();
        $categories = Category::orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.categories.index', compact('categories', 'serial'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }

//    public function store(CategoryRequest $request)
    public function store(Request $request)
    {
//        $category = new Category();
//        $category->title = $request->title;
//        $category->save();

//        $request->validate([
//            'title' => 'required|max:30|min:5|unique:categories',
//        ]);


        $validator = Validator::make($request->all(), [
            'title' => 'required|max:30|min:5|unique:categories',
            'image' => 'mimes:png,jpeg,pdf',
        ]);

        if ($validator->fails()) {
            return redirect('categories/create')
                ->withErrors($validator)
                ->withInput();
        }
        try{
            $data = $request->all();
            if($request->hasFile('image')){
                $data['image'] = $this->uploadImage($request->image, $request->title);
            }
            Category::create($data);
//        session()->flash('message', 'Task was successful!');

            return redirect()
                ->route('categories.index')
                ->withMessage('Task was successful!');
        }catch (QueryException $e){
            return redirect()
                ->route('categories.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }


    }

//    public function show($id)
    public function show(Category $category)//route model binding
    {
//        $category = Category::findOrFail($id);
        return view('backend.categories.show', compact('category'));
    }

//    public function edit($id)
    public function edit(Category $category)
    {
//        $category = Category::findOrFail($id);
        return view('backend.categories.edit', compact('category'));
    }

//    public function update(CategoryRequest $request, $id)
    public function update(CategoryRequest $request, Category $category)
    {
//        $category = Category::findOrFail($id);
//        $category = new Category();
//        $category->title = $request->title;
//        $category->save();

//        $request->validate([
//            'title' => 'required|max:30|min:5|unique:categories',
//        ]);

//            $validator = Validator::make($request->all(), [
//                'title' => 'required|max:30|min:5|unique:categories',
////            'body' => 'required',
//            ]);
//
//            if ($validator->fails()) {
//                return redirect('categories/create')
//                    ->withErrors($validator)
//                    ->withInput();
//            }

        $data = $request->all();
        if($request->hasFile('image')){
            $data['image'] = $this->uploadImage($request->image, $request->title);
        }else{
            $data['image'] = $category->image;
        }
        $category->update($data);
//        session()->flash('message', 'Task was successful!');
            return redirect()
                ->route('categories.index')
                ->withMessage('Task was successful!');
    }

//    public function destroy($id)
    public function destroy(Category $category)
    {
//        $category = Category::findOrFail($id);
        $category->delete();
        return redirect()
            ->route('categories.index')
            ->withMessage('Task was successful!');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pageNumber = request('page');
        if($pageNumber > 1){
            $serial = ($pageNumber * $paginatePerPage) - $paginatePerPage;
        }else{
            $serial = 0;
        }
//        $categories = Category::all();
        $categories = Category::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.categories.trash', compact('categories', 'serial'));

    }

    public function restore($id)
    {
        $category = Category::onlyTrashed()
                                ->where('id', $id)
                                ->first();
        $category->restore();
        return redirect()
            ->route('categories.trash')
            ->withMessage('Task was successful!');

    }

    public function delete($id)
    {
        $category = Category::onlyTrashed()
            ->where('id', $id)
            ->first();
        $category->forceDelete();
        return redirect()
            ->route('categories.trash')
            ->withMessage('Task was successful!');

    }


    private function uploadImage($file, $title)
    {
        $extension = $file->getClientOriginalExtension($file);
        $fileName = date('y-m-d').'_'.time().'_'.$title.'.'.$extension;
//        $file->move(public_path().self::UPLOAD_DIR, $fileName);
        Image::make($file)->resize(100,100)->save(public_path().self::UPLOAD_DIR.$fileName);
        return $fileName;
    }
}
