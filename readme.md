
## Installation
- git clone `https://gitlab.com/mdahosanhabib/pondit-intern1.git`
- `composer update`
- `cp .env.example .env`
- `php artisan key:generate`
- `php artisan serve`
