@extends('backend.layouts.master')

@section('content')
    <div class="main-grid">
        <div class="agile-grids">
            <!-- input-forms -->
            <div class="grids">
                <div class="progressbar-heading grids-heading">
                    <h2>Categories</h2>
                    <a href="{{ route('categories.index') }}">List</a>
                </div>
                <div class="panel panel-widget forms-panel">
                    <div class="forms">
                        <div class="form-grids widget-shadow" data-example-id="basic-forms">
                            <div class="form-title">
                                <h4>Category Edit Form</h4>
                            </div>
                            <div class="form-body">
                                @include('backend.layouts.elements.errors')
                                {{--                                <form action="{{ url('categories/', $category->id) }}" method="post">--}}
                                {{--<form action="{{ route('categories.update', $category->id) }}" method="post">--}}
                                    {{--@csrf--}}
                                    {{--@method('put')--}}
                                    {{ Form::model($category,[
                                        'route' => ['categories.update', $category->id],
                                        'method' => 'put'
                                    ]) }}
                                        @include('backend.categories.form')
                                {{--</form>--}}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //input-forms -->
        </div>
    </div>
@endsection
