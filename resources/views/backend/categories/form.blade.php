<div class="form-group">
    {{--<label for="title">Title</label>--}}
    {{ Form::label('Title') }}
    {{--<input type="text"  name="title" value="@isset($category->title) {{ $category->title }} @else {{ old('title') }} @endisset" class="form-control" id="title" placeholder="Title">--}}
    {{ Form::text('title', null, [
        'class' => 'form-control',
        'id' => 'title',
        'placeholder' => 'Enter Title',
    ]) }}
</div>
<div class="form-group">
    {{ Form::label('image') }}
    {{ Form::file('image', null, [
        'class' => 'form-control',
        'id' => 'image',
    ]) }}
</div>
{{--<button type="submit" class="btn btn-primary w3ls-button">Submit</button>--}}
{{ Form::button('Submit', [
    'type' => 'submit',
    'class' => 'btn btn-primary w3ls-button',
]) }}
