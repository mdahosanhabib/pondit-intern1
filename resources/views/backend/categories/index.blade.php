@extends('backend.layouts.master')

@section('content')
<div class="table-heading">
    <h2>Categories</h2>
    <a href="{{ route('categories.create') }}" class="btn btn-primary">Create</a>
    <a href="{{ route('categories.trash') }}" class="btn btn-info">Trash</a>
</div>
<div class="agile-tables">
    <div class="w3l-table-info">
        <h3>List</h3>

        @include('backend.layouts.elements.message')
        @include('backend.layouts.elements.errors')

        <table class="table table-striped table-responsive table-bordered">
            <thead>
            <tr class="bg-info">
                <th>SL#</th>
                <th>Title</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                {{--@php $sl = 0 @endphp--}}
                @foreach($categories as $category)
                    <tr>
                        <td>{{ ++$serial }}</td>
                        <td>{{ $category->title }}</td>
                        <td>
                            @if(file_exists(public_path().'/uploads/categories/'.$category->image) && (!is_null($category->image)))
                                 <img src="{{ asset('/uploads/categories/'.$category->image) }}" height="100">
                            @else
                                <img src="{{ asset('/uploads/default.png') }}" height="100">
                            @endif
                        </td>
                        <td>
{{--                            <a href="{{ url('/categories/'.$category->id) }}">show</a>--}}
                            <a href="{{ route('categories.show', $category->id) }}">show</a>
{{--                            <a href="{{ url('/categories/'.$category->id.'/edit') }}">Edit</a>--}}
                            <a href="{{ route('categories.edit', $category->id) }}">Edit</a>
                            <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                               @csrf
                               @method('delete')
                               <button type="submit" onclick="return confirm('Are You Sure Want To Delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                 @endforeach
            </tbody>
        </table>
        {{ $categories->links() }}
    </div>
</div>
@endsection

{{--@push('script')--}}
    {{--<script>--}}
        {{--$('#table').basictable();--}}
    {{--</script>--}}
{{--@endpush--}}

{{--@push('css')--}}
    {{--<!-- tables -->--}}
    {{--<link rel="stylesheet" type="text/css" href="css/table-style.css" />--}}
    {{--<link rel="stylesheet" type="text/css" href="css/basictable.css" />--}}
    {{--<script type="text/javascript" src="js/jquery.basictable.min.js"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function() {--}}
            {{--$('#table').basictable();--}}

            {{--$('#table-breakpoint').basictable({--}}
                {{--breakpoint: 768--}}
            {{--});--}}

            {{--$('#table-swap-axis').basictable({--}}
                {{--swapAxis: true--}}
            {{--});--}}

            {{--$('#table-force-off').basictable({--}}
                {{--forceResponsive: false--}}
            {{--});--}}

            {{--$('#table-no-resize').basictable({--}}
                {{--noResize: true--}}
            {{--});--}}

            {{--$('#table-two-axis').basictable();--}}

            {{--$('#table-max-height').basictable({--}}
                {{--tableWrapper: true--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    {{--<!-- //tables -->--}}
{{--@endpush--}}
