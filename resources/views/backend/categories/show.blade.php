@extends('backend.layouts.master')

@section('content')
    <div class="card bg-white">
        <div class="card-header bg-info">
            <h3>Category Details</h3>

            <a href="{{ route('categories.index') }}">List</a>
        </div>
        <div class="card-body">
            <p><strong>Title : </strong> {{ $category->title }}</p>

        </div>
        <div class="card-footer"></div>
    </div>



@endsection
