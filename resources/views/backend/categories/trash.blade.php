@extends('backend.layouts.master')

@section('content')
    <div class="table-heading">
        <h2>Deleted Categories</h2>
        <a href="{{ route('categories.index') }}" class="btn btn-info">List</a>
    </div>
    <div class="agile-tables">
        <div class="w3l-table-info">
            <h3>List</h3>
            @include('backend.layouts.elements.message')
            @include('backend.layouts.elements.errors')
            <table class="table table-striped table-responsive table-bordered">
                <thead>
                <tr class="bg-info">
                    <th>SL#</th>
                    <th>Title</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {{--@php $sl = 0 @endphp--}}
                @foreach($categories as $category)
                    <tr>
                        <td>{{ ++$serial }}</td>
                        <td>{{ $category->title }}</td>
                        <td>
                            {{--                            <a href="{{ url('/categories/'.$category->id) }}">show</a>--}}
{{--                            <a href="{{ route('categories.show', $category->id) }}">show</a>--}}
                            {{--                            <a href="{{ url('/categories/'.$category->id.'/edit') }}">Edit</a>--}}
                            <a href="{{ route('categories.restore', $category->id) }}">Restore</a>
                            <form action="{{ route('categories.delete', $category->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" onclick="return confirm('Are You Sure Want To Delete It Permanently?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $categories->links() }}
        </div>
    </div>
@endsection
