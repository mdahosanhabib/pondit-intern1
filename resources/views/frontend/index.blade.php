@extends('frontend.layouts.master')


@section('content')
<!--/ab -->
<section class="about py-md-5 py-5">
    <div class="container-fluid">
        <div class="feature-grids row px-3">
            <div class="col-lg-3 gd-bottom">
                <div class="bottom-gd row">
                    <div class="icon-gd col-md-3 text-center"><span class="fa fa-truck" aria-hidden="true"></span></div>
                    <div class="icon-gd-info col-md-9">
                        <h3 class="mb-2">FREE SHIPPING</h3>
                        <p>On all order over $2000</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 gd-bottom">
                <div class="bottom-gd row bottom-gd2-active">
                    <div class="icon-gd col-md-3 text-center"><span class="fa fa-bullhorn" aria-hidden="true"></span></div>
                    <div class="icon-gd-info col-md-9">
                        <h3 class="mb-2">FREE RETURN</h3>
                        <p>On 1st exchange in 30 days</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 gd-bottom">
                <div class="bottom-gd row">
                    <div class="icon-gd col-md-3 text-center"> <span class="fa fa-gift" aria-hidden="true"></span></div>

                    <div class="icon-gd-info col-md-9">
                        <h3 class="mb-2">MEMBER DISCOUNT</h3>
                        <p>Register & save up to $29%</p>
                    </div>

                </div>
            </div>
            <div class="col-lg-3 gd-bottom">
                <div class="bottom-gd row">
                    <div class="icon-gd col-md-3 text-center"> <span class="fa fa-usd" aria-hidden="true"></span></div>
                    <div class="icon-gd-info col-md-9">
                        <h3 class="mb-2">PREMIUM SUPPORT</h3>
                        <p>Support 24 hours per day</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //ab -->
<!--/ab -->
<section class="about py-5">
    <div class="container pb-lg-3">
        <h3 class="tittle text-center">New Arrivals</h3>
        <div class="row">
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s1.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="{{ route('single') }}">Bella Toes </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$675.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s2.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Chikku Loafers </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$475.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s3.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">(SRV) Sneakers </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$575.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men my-lg-4">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s4.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Shuberry Heels</a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$575.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men my-lg-4">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s5.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Red Bellies </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$575.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men my-lg-4">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s6.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Catwalk Flats </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$575.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s7.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Running Shoes </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$675.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s8.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Sukun Casuals </a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price ">
                                <span class="money ">$775.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 product-men">
                <div class="product-shoe-info shoe text-center">
                    <div class="men-thumb-item">
                        <img src="{{ asset('/ui/frontend/images/s9.jpg') }}" class="img-fluid" alt="">
                        <span class="product-new-top">New</span>
                    </div>
                    <div class="item-info-product">
                        <h4>
                            <a href="shop-single.html">Bank Sneakers</a>
                        </h4>

                        <div class="product_price">
                            <div class="grid-price">
                                <span class="money">$875.00</span>
                            </div>
                        </div>
                        <ul class="stars">
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a></li>
                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- //ab -->
<!--/testimonials-->
<section class="testimonials py-5">
    <div class="container">
        <div class="test-info text-center">
            <h3 class="my-md-2 my-3">Jenifer Burns</h3>

            <ul class="list-unstyled w3layouts-icons clients">
                <li>
                    <a href="#">
                        <span class="fa fa-star"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-star"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-star"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="fa fa-star-half-o"></span>
                    </a>
                </li>
            </ul>
            <p><span class="fa fa-quote-left"></span> Lorem Ipsum has been the industry's standard since the 1500s. Praesent ullamcorper dui turpis.Nulla pellentesque mi non laoreet eleifend. Integer porttitor mollisar lorem, at molestie arcu pulvinar ut. <span class="fa fa-quote-right"></span></p>

        </div>
    </div>
</section>
<!--//testimonials-->
<!--/ab -->
<section class="about py-5">
    <div class="container pb-lg-3">
        <h3 class="tittle text-center">Popular Products</h3>
        <div class="row">
            <div class="col-md-6 latest-left">
                <div class="product-shoe-info shoe text-center">
                    <img src="{{ asset('/ui/frontend/images/img1.jpg') }}" class="img-fluid" alt="">
                    <div class="shop-now"><a href="shop.html" class="btn">Shop Now</a></div>
                </div>
            </div>
            <div class="col-md-6 latest-right">
                <div class="row latest-grids">
                    <div class="latest-grid1 product-men col-12">
                        <div class="product-shoe-info shoe text-center">
                            <div class="men-thumb-item">
                                <img src="{{ asset('/ui/frontend/images/img2.jpg') }}" class="img-fluid" alt="">
                                <div class="shop-now"><a href="shop.html" class="btn">Shop Now</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="latest-grid2 product-men col-12 mt-lg-4">
                        <div class="product-shoe-info shoe text-center">
                            <div class="men-thumb-item">
                                <img src="{{ asset('/ui/frontend/images/img3.jpg') }}" class="img-fluid" alt="">
                                <div class="shop-now"><a href="shop.html" class="btn">Shop Now</a></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //ab -->
@include('frontend.layouts.elements.brands')
@endsection
