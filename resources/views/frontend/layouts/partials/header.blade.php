<div class="main-banner" id="home">
    <!-- header -->
    <header class="header">
        <div class="container-fluid px-lg-5">
            <!-- nav -->
            <nav class="py-4">
                <div id="logo">
                    <h1> <a href="index.html"><span class="fa fa-bold" aria-hidden="true"></span>ootie</a></h1>
                </div>

                <label for="drop" class="toggle">Menu</label>
                <input type="checkbox" id="drop" />
                <ul class="menu mt-2">
                    <li class="active"><a href="index.html">Home</a></li>
                    <li><a href="{{ route('about') }}">About</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li>
                        <!-- First Tier Drop Down -->
                        <label for="drop-2" class="toggle">Drop Down <span class="fa fa-angle-down" aria-hidden="true"></span> </label>
                        <a href="#">Drop Down <span class="fa fa-angle-down" aria-hidden="true"></span></a>
                        <input type="checkbox" id="drop-2" />
                        <ul>
                            <li><a href="blog.html">Blog</a></li>
                            <li><a href="shop.html">Shop Now</a></li>
                            <li><a href="shop-single.html">Single Page</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav>
            <!-- //nav -->
        </div>
    </header>
    <!-- //header -->
    <!--/banner-->
    <div class="banner-info">
        <p>Trending of the week</p>
        <h3 class="mb-4">Casual Shoes for Men</h3>
        <div class="ban-buttons">
            <a href="shop-single.html" class="btn">Shop Now</a>
            <a href="single.html" class="btn active">Read More</a>
        </div>
    </div>
    <!--// banner-inner -->

</div>
