<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@welcome');
Route::get('/about', 'FrontendController@about')->name('about');
Route::get('/single', 'FrontendController@single')->name('single');
Route::get('/admin', 'DashboardController@dashboard')->name('dashboard');

//Route::prefix('categories')->group(function () {
//    Route::get('/', 'CategoryController@index')->name('categories.index');
//    Route::get('/create', 'CategoryController@create')->name('categories.create');
//    Route::post('/', 'CategoryController@store')->name('categories.store');
//    Route::get('/{id}', 'CategoryController@show')->name('categories.show');
//    Route::get('/{id}/edit', 'CategoryController@edit')->name('categories.edit');
//    Route::put('/{id}', 'CategoryController@update')->name('categories.update');
//    Route::delete('/{id}', 'CategoryController@destroy')->name('categories.destroy');
//});

Route::get('categories/trash', 'CategoryController@trash')->name('categories.trash');
Route::get('categories/{id}/restore', 'CategoryController@restore')->name('categories.restore');
Route::delete('categories/{id}/delete', 'CategoryController@delete')->name('categories.delete');
Route::resource('/categories', 'CategoryController');


//Route::get('categories/{id}/edit', 'CategoryController@edit')->name('categories.edit');


